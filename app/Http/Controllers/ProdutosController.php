<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormRequestProduto;
use App\Models\Produto;
use Illuminate\Http\Request;
use JetBrains\PhpStorm\Internal\ReturnTypeContract;

use function Laravel\Prompts\search;

class ProdutosController extends Controller
{
    protected $produto;

    //metado construtor 
    public function __construct(Produto $produto)
    
    {
        $this->produto = $produto;
    }
    
    
    public function index (Request $request) 
    {
        $pesquisar = $request->input('pesquisar');
        $findProduto = $this->produto->getProdutosPesquisarIndex(search: $pesquisar ?? '');
         // se ele for num ele retorna um string vazia '' //Produto::all(); //vai buscar todo mundo  //Produto::all pega tudo 
       //dd($findProduto);  //vai debgar e morre / perigoso em produção/ não reproduz para baixo

        return view('pages.produtos.paginacao', compact('findProduto')); //vai retornar a a aba viewer 

    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $buscaRegistro = Produto::find($id);
        $buscaRegistro->delete();

       return response()->json(['sucess' => true]); 
    }

    public function cadastrarProduto(FormRequestProduto $request) 
    {
            if ($request->method() == "POST"); {
                // cria os dados
                $data = $request->all();
                Produto::create($data);

                return redirect()->route('produto.index');   //se gravou e deu certo manda para produto.index
            }

            return view('pages.produtos.create');
    }

}

