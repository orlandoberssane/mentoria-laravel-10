        {{--Extends da Index--}}
        @extends('index') 

        {{--Extends da section/ ele vai deixa trocar de pagina apenas a parte do meio e menu fica--}}
        @section('content')
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Produtos</h1>      
          </div>
            <div>
              <form action="{{route('produto.index')}}" method="get">      {{-- Vai buscar no produtos controlex index --}}
                <input type="text" name="pesquisar" placeholder="Pesquisar produtos..." />
                <button> Pesquisar </button>
                    <a type="button" href=" {{ route ('cadastrar.produto') }}" class="btn btn-success float-end">
                      Incluir Produto 
                    </a>       
              </form>

              <div class="table-responsive mt-4">
                @if ($findProduto->isEmpty())  {{-- quando for pesquisar e não existir dados, vai volta a msg não existe dados  --}}
                  <p> Não Existe Dados </p>
                  @else
                      <table class="table table-striped table-sm">
                          <thead>
                            <tr>
                              <th>Nome</th>
                              <th>Valor</th>
                              <th>Ações</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($findProduto as $produto)
                            <tr>
                              <td>{{ $produto->nome }}</td>
                              <td>{{'R$' . ' ' . number_format($produto->valor, 2, ',', '.') }}</td> {{-- vai formatar de . para ,, virgula --}}
                              <td>
                                <a href="" class="btn btn-light btn-sm">
                                  Editar
                                </a>

                                <meta name='csrf-token' content=" {{ csrf_token() }}" /> {{--criando token autorizaçáo para fazer requer --}}
                                <a onclick="deleteRegistroPaginacao( '{{ route ('produto.delete') }} ', {{ $produto->id }} )" class="btn btn-danger btn-sm">
                                  Excluir
                                </a>
                              </td>
                            </tr>                  
                            @endforeach               
                          </tbody>
                        </table> 
                  @endif
              </div> 
            </div>
        @endsection
          
